# Deploy eBGP to FRR with Ansible

Before using this script, deploy https://github.com/CumulusNetworks/cldemo-vagrant

After that, download this repo to oob-mgmt-server and run it with ansible.

In this topology, leaf01 and leaf02 are treated as TORs within the same rack, so they have the same ASN. The same goes for leaf03 and leaf04.

This ansible playbook will:
* connect leafs to servers, with LACP
* connect all switches with BGP, using a VRF called "server-net"
* connect exit nodes and internet node to BGP, and share public route via BGP

## Steps for running the playbook

1. Connect to the internet node and change the sudoers settings for the sudo group so that it can run commands without password:

```
cumulus@internet:~$ sudo cat /etc/sudoers | grep "%sudo"
%sudo	ALL=(ALL:ALL) NOPASSWD:ALL
```

2. From *oob-mgmt-server*, connect to all servers and install python-apt:

```
cat /etc/hosts | grep server0 | awk '{ print $2 }' | xargs -i ssh {} sudo apt install python-apt -y
```

3. Clone the repo and deploy the config:

```
git clone https://gitlab.com/gun1x/frr_ansible_ebgp
cd frr_ansible_ebgp
ansible-playbook -i hosts.ini deploy.yaml
```

4. Restart all nodes:

```
cat /etc/hosts | grep -v mgmt | grep 192.168 | awk '{ print $2 }' | xargs -i ssh {} sudo shutdown -r
```
5. Test connectivity:

```
ansible-playbook -i hosts.ini test.yaml
```

## CI/CD

CI/CD has been configured for this repo. This means, every time changes are applied to the repo, it get executed in a test environment.

In order to have your CI/CD work with this setup, you will need the following:
* a gitlab runner that can access the environment
* ssh keys on so that the user that runs the playbooks can connect to all nodes

A quick solved for this is to create a VM in the vagrant network, and to route all traffic through oob-mgmt-server. Here is an example of the routing table I use on my runner server:
```
[gunix@gitlab-runner.gunix.test][~]% ip route 
default via 192.168.121.1 dev ens3 proto dhcp src 192.168.121.145 metric 202 
192.168.0.0/24 via 192.168.121.109 dev ens3 
192.168.121.0/24 dev ens3 proto dhcp scope link src 192.168.121.145 metric 202 
```
In this case, 192.168.121.109 is oob-mgmt-server.

